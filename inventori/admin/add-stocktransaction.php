<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$resultWarehouse = mysqli_query($koneksi, "SELECT * FROM warehouse");
$resultEmployee = mysqli_query($koneksi, "SELECT * FROM employee");
$resultProduct = mysqli_query($koneksi, "SELECT * FROM product");
$resultProduct2 = mysqli_query($koneksi, "SELECT * FROM product");

$dataJson = "ss";
$list = array(); 
$item = array();
$count = 0;
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>
</head>
<body>
	<div class="row">
		<!--header-->
		<?php include 'header.php';?>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
								<td>Date<?php $dataJson ?></td>
								<td><input type="date" name="Date" required></td>
							</tr>
							<tr>
								<td>Employee</td>
								<td>
                                    <select name="EmployeeId" class="browser-default">
                                        <?php
                                        while($row = mysqli_fetch_array($resultEmployee))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['EmployeeName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Warehouse</td>
								<td>
                                    <select name="WarehouseId" class="browser-default">
                                        <?php
                                        while($row = mysqli_fetch_array($resultWarehouse))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['WarehouseName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Keterangan</td>
								<td><input type="text" name="Description" required></td>
							</tr>
						</table>
						<hr>
						<h3>Items</h3>
						<hr>
						<div>
							<table>
								<thead>
									<th>Product</th>
									<th>Quantity</th>
								</thead>
								<tbody id="body_logic">
									<tr>
										<td>
											<select name="ProductId" class="browser-default">
												<?php
												while($row = mysqli_fetch_array($resultProduct))
												{?>
												<option value = "<?php echo($row['Id'])?>" >
													<?php echo($row['ProductName']) ?>
												</option>
												<?php }?>
											</select>
											<?php
												while($row2 = mysqli_fetch_array($resultProduct2)){
													$obj = new stdClass;
													$obj->Id = $row2['Id'];
													$obj->ProductCode = $row2['ProductCode'];
													$obj->ProductName = $row2['ProductName'];
													$obj->UnitId = $row2['UnitId'];
													array_push($list, $obj);
												}
												$count = count($list);
												// $dataJson = json_encode($list);
												// echo $dataJson;

												// foreach($list as $struct2) {
												// 	echo $struct2->ProductCode;
												//   }
											?>
										</td>
										<td>
											<input type="number" name="Quantity" required>
										</td>
									</tr>
								<tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<input id="add_row" type="button" name="tambahitem" value="Add" class="right waves-effect waves-light btn green darken-2" style="float: left;">
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
						<table>
							<tr>
								<th>
									<input type="submit" name="tambah" value="Simpan" class="right waves-effect waves-light btn green darken-2" style="float: left;">
								</th>
								<th style="width: 1%;">
									<a href="employees.php"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
								</th>
							</tr>
				    	</table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

        <!-- Proses Penambahan Data User -->

        <?php
 
		  if(isset($_POST['tambahitem']))
		  {
			$newItem = array("ProductId" => null,
			"Quantity" => 0);

			array_push($listStockTransactionItem, $newItem);
			$count = count($listStockTransactionItem);
			$jsonData = json_encode($listStockTransactionItem);
		  }

          // Check If form submitted, insert form data into users table.
          if(isset($_POST['tambah'])) {
            $employeeCode = $_POST['EmployeeCode'];
            $employeeName = $_POST['EmployeeName'];
            
            // include database connection file
            include_once("../config.php");
                
            // Insert user data into table
            $result = mysqli_query($koneksi, "INSERT INTO stocktransaction(EmployeeCode,EmployeeName) VALUES('$employeeCode','$employeeName')");
            
            echo "<script>alert('Tambah Employee Berhasil')</script>";
            header("Location: employees.php");
          }
        ?>

        <!-- End -->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		}),
		$(document).ready(function(){
	    	$('#add_row').click(function() {
				$('#body_logic')
					.append('<tr>')
				// $('#body_logic')
					// .append('<td><select name="ProductId" class="browser-default"><?php while($row = mysqli_fetch_array($resultProduct)) {?> <option value = "<?php echo($row['Id'])?>" > <?php echo($row['ProductName']) ?> </option> <?php }?> </select></td>')
				$('#body_logic')
					.append('<td><select name="ProductId" class="browser-default"><?php foreach($list as $item) {?> <option value = "<?php echo($item->Id)?>" > <?php echo($item->ProductName) ?> </option> <?php }?> </select></td>')
				$('#body_logic')
					.append('<td><input type="number" name="Quantity" required></td>')
				$('#body_logic')
					.append('</tr>');
			});
		})
	</script>
</body>
</html>