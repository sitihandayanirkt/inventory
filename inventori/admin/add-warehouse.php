<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$url = dirname(__FILE__) ."\\warehouses.php";

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>
</head>
<body>
	<div class="row">
		<!--header-->
		<?php include 'header.php';?>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
								<th>Warehouse Code<?php echo $url ?></th>
								<th><input type="text" name="WarehouseCode" required></th>
							</tr>
							<tr>
								<th>Name</th>
								<th><input type="text" name="WarehouseName" required></th>
							</tr>
						</table>
						<table>
							<tr>
								<th>
									<input type="submit" name="tambah" value="Tambah Warehouse" class="right waves-effect waves-light btn green darken-2" style="float: left;">
								</th>
								<th style="width: 1%;">
									<a href="warehouses.php"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
								</th>
							</tr>
				    </table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

        <!-- Proses Penambahan Data User -->

        <?php
 
          // Check If form submitted, insert form data into users table.
          if(isset($_POST['tambah'])) {
            $warehouseCode = $_POST['WarehouseCode'];
            $warehouseName = $_POST['WarehouseName'];
            
            // include database connection file
            include_once("../config.php");
                
            // Insert user data into table
            $result = mysqli_query($koneksi, "INSERT INTO warehouse(WarehouseCode,WarehouseName) VALUES('$warehouseCode','$warehouseName')");
            
						echo "<script>alert('Tambah Warehouse Berhasil')</script>";
						echo '<script type="text/javascript">window.location=\''.$url.'\';</script‌​>';
						// exit();
          }
        ?>

        <!-- End -->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		});
	</script>
</body>
</html>