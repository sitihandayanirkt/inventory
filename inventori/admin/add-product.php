<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$resultUnit = mysqli_query($koneksi, "SELECT * FROM unit");

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>
</head>
<body>
	<div class="row">
		<!--header-->
		<?php include 'header.php';?>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
								<th>Product Code</th>
								<th><input type="text" name="ProductCode" required></th>
							</tr>
							<tr>
								<th>Name</th>
								<th><input type="text" name="ProductName" required></th>
							</tr>
                            <tr>
								<th>Unit</th>
								<th>
                                    <select name="UnitId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultUnit))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['UnitName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </th>
							</tr>
						</table>
						<table>
							<tr>
								<th>
									<input type="submit" name="tambah" value="Tambah Product" class="right waves-effect waves-light btn green darken-2" style="float: left;">
								</th>
								<th style="width: 1%;">
									<a href="products.php"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
								</th>
							</tr>
				    </table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

        <!-- Proses Penambahan Data User -->

        <?php
 
          // Check If form submitted, insert form data into users table.
          if(isset($_POST['tambah'])) {
            $productCode = $_POST['ProductCode'];
            $productName = $_POST['ProductName'];
            $unitId = $_POST['UnitId'];
            
            // include database connection file
            include_once("../config.php");

            $unitQuery = mysqli_query($koneksi, "SELECT * FROM unit WHERE Id=$unitId");
            $unit = mysqli_fetch_array($unitQuery);
                
            // Insert user data into table
            $result = mysqli_query($koneksi, "INSERT INTO product(ProductCode,ProductName,UnitId,UnitCode,UnitName) VALUES('$productCode','$productName', '$unitId', '$unit[UnitCode]', '$unit[UnitName]')");
            
            echo "<script>alert('Tambah Product Berhasil')</script>";
            header("Location: products.php");
          }
        ?>

        <!-- End -->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		});
	</script>
</body>
</html>