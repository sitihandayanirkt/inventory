<?php session_start();
include_once("../config.php");
$result = mysqli_query($koneksi, "SELECT * FROM users ORDER BY nik DESC");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';

?>
<?php
// include database connection file
include_once("../config.php");
 
// Check if form is submitted for user update, then redirect to homepage after update
if(isset($_POST['update']))
{ 
  $id = $_POST['Id'];
  $unitName = $_POST['UnitName'];
  $unitCode=$_POST['UnitCode'];
    
  // update user data
  $result = mysqli_query($koneksi, "UPDATE unit SET UnitName='$unitName',UnitCode='$unitCode' WHERE Id=$id");
  
  // Redirect to homepage to display updated user in list
  header("Location: units.php");
}
?>
<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];
 
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM unit WHERE Id=$id");
 
while($user_data = mysqli_fetch_array($result))
{
  $unitName = $user_data['UnitName'];
  $unitCode = $user_data['UnitCode'];
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<?php include 'header.php';?>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
					        	<th>Unit Code</th>
					        	<th><input readonly type="text" name="UnitCode" value=<?php echo $unitCode; ?>></th>
					      	</tr>
					      	<tr> 
					        	<td>Name</td>
					        	<td><input type="text" name="UnitName" value=<?php echo $unitName;?>></td>
					      	</tr>
					      	<tr>
				            	<td><input type="hidden" name="Id" value=<?php echo $_GET['id'];?>></td>
				            </tr>
					      	</table>
					      	<table>
				            <tr>
				            	<th>
				            		<input type="submit" name="update" value="Edit Unit" class="right waves-effect waves-light btn green darken-2" style="float: left;">
				            	</th>
				            	<th style="width: 1%;">
				            		<a href="unit.php"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
				            	</th>
				            </tr>
				        </table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		});
	</script>
</body>
</html>