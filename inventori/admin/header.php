<header>
    <!--TopNav-->
    <nav class="row top-nav red darken-2">
        <div class="container">
            <div class="col offset-l2 nav-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
                <a class="page-title">Beranda</a>
            </div>
        </div>
    </nav>

    <!--Sidenav-->
    <ul id="slide-out" class="side-nav fixed">
        
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li>
                    <div class="user-view">
                        <div class="background" style="margin-bottom:-15%;">
                            <img src="../images/bg.jpg">
                        </div>
                        <span class="white-text name"><?php echo $nama; ?><i class="material-icons left">account_circle</i></span>
                    </div>
                </li>
                
                <li><div class="divider" style="margin-top:15%;"></div></li>

                <li><a class="collapsible-header">Beranda<i class="material-icons">home</i></a></li>

                <li>
                    <a class="collapsible-header">Menu<i class="material-icons">arrow_drop_down</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="user.php">User</a></li>
                            <li><a href="employees.php">Employee</a></li>
                            <li><a href="units.php">Unit</a></li>
                            <li><a href="products.php">Product</a></li>
                            <li><a href="warehouses.php">Warehouse</a></li>
                            <li><a href="stocktransaction.php">Stock Transaction IN</a></li>
                            <li><a href="barangmasuk.php">Barang Masuk</a></li>
                            <li><a href="gudang.php">Stok Gudang ATK</a></li>
                            <li><a href="barangkeluar.php">Permintaan ATK</a></li>
                            <li><a href="inventaris.php">Inventaris</a></li>
                            <li><a href="barangkeluar.php">Laporan</a></li>
                        </ul>
                    </div>
                </li>

                <li><a href="../logout.php" class="collapsible-header">Keluar<i class="material-icons">exit_to_app</i></a></li>

            </ul>
        </li>

    </ul>
</header>