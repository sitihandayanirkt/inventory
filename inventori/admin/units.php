<?php session_start();
include_once("../config.php");
$result = mysqli_query($koneksi, "SELECT * FROM unit ORDER BY UnitCode");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$valueCari = '';

if(isset($_POST['cari']))
{
	$cari = $_POST['cari'];
	$valueCari = $cari;
	$pnjng = 100;
	$substr = substr($cari, 0, $pnjng).' ...';
	if($cari != null && $cari != '')
		$sql = "SELECT * FROM unit WHERE UnitName LIKE '%$cari%' OR UnitCode LIKE '%$cari%'";
	else
		$sql = "SELECT * FROM unit";

	$result = mysqli_query($koneksi,$sql);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<?php include 'header.php';?>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l12 offset-l2"> <br>
					<!--kolom search-->
					<div class="col s12 m12 l12">
						<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="row">
							<div class="e-input-field col s12 m12 l12">
								<input value="<?php echo $valueCari ?>" name="cari" onchange="this.form.submit();" type="text" name="cari" placeholder="Cari Berdasarkan Unit Code / Nama" class="validate" required title="Cari User">
								<input type="submit" name="cari2" value="cari" class="btn right red darken-2"> 
							</div>
						</form>
					</div>

					<!--table-->
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom header table-->
							<tr>
			                    <th hidden>ID</th>
								<th>Kode</th>
								<th>Nama</th>
				            </tr>

							<?php 

							while($user_data = mysqli_fetch_array($result)) 
							{ 
									echo "<tr>";
									echo "<td hidden>".$user_data['Id']."</td>";
									echo "<td> <h6>".$user_data['UnitCode']."</h6></td>";
									echo "<td> <h6>".$user_data['UnitName']."</h6> </td>";
									echo "<td> <a href='edit-unit.php?id=$user_data[Id]' style='text-decoration: none;'>
																<i class='material-icons' title='Edit'>mode_edit</i>
														 </a> | 
														 <a data-id='1' class='hapus' href='delete-unit.php?id=$user_data[Id]' style='text-decoration: none;'>
																<i class='material-icons' title='Hapus'>delete</i>
															</a>
												</td>";
									echo "</tr>";
							}

							?>
							
						</table>
						<table>
							<tr>
								<td colspan='9'>
									<a href='add-unit.php' class="right waves-effect waves-light btn red darken-2">Add Unit<i class="material-icons right">add</i></a>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</main>
        <!--end of content-->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
			});
	</script>
	<script>
        $(".hapus").click(function () {
        var jawab = confirm("Anda Yakin Ingin Menghapus Unit ?");
        if (jawab === true) {
        // konfirmasi
            var hapus = false;
            if (!hapus) {
                hapus = true;
                $.post('delete-unit.php', {id: $(this).attr('data-id')},
                function (data) {
                    alert(data);
                });
                hapus = false;
            }
        } else {
            return false;
        }
        });
      </script>
</body>
</html>