-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2019 at 10:07 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `Id` int(10) NOT NULL,
  `EmployeeCode` varchar(100) NOT NULL,
  `EmployeeName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`Id`, `EmployeeCode`, `EmployeeName`) VALUES
(1, '001', 'Employee1'),
(4, '002', 'Employee2');

-- --------------------------------------------------------

--
-- Table structure for table `lending`
--

CREATE TABLE `lending` (
  `Id` int(10) NOT NULL,
  `LendingNo` varchar(100) NOT NULL,
  `CollectionDate` date NOT NULL,
  `ReturnDate` date NOT NULL,
  `EmployeeId` int(10) NOT NULL,
  `EmployeeName` varchar(100) NOT NULL,
  `EmployeeCode` varchar(100) NOT NULL,
  `BorrowerId` int(10) NOT NULL,
  `BorrowerCode` varchar(100) NOT NULL,
  `BorrowerName` varchar(100) NOT NULL,
  `ProductId` int(10) NOT NULL,
  `ProductCode` varchar(100) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `UnitId` int(10) NOT NULL,
  `UnitCode` varchar(100) NOT NULL,
  `UnitName` varchar(100) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Status` enum('Active','Completed') NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lending`
--

INSERT INTO `lending` (`Id`, `LendingNo`, `CollectionDate`, `ReturnDate`, `EmployeeId`, `EmployeeName`, `EmployeeCode`, `BorrowerId`, `BorrowerCode`, `BorrowerName`, `ProductId`, `ProductCode`, `ProductName`, `UnitId`, `UnitCode`, `UnitName`, `Quantity`, `Status`, `Description`) VALUES
(1, '000001\r\n', '2019-03-24', '2019-03-31', 1, 'Employee1', '001', 4, '002', 'Employee2', 4, '001', 'Product1', 2, 'lsn', 'lusin', 2, 'Completed', '');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `Id` int(10) NOT NULL,
  `ProductCode` varchar(100) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `UnitId` int(10) NOT NULL,
  `UnitCode` varchar(100) NOT NULL,
  `UnitName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`Id`, `ProductCode`, `ProductName`, `UnitId`, `UnitCode`, `UnitName`) VALUES
(4, '001', 'Product1', 2, 'lsn', 'lusin'),
(5, '002', 'Product2', 1, 'pcs', 'pcs');

-- --------------------------------------------------------

--
-- Table structure for table `sequenceno`
--

CREATE TABLE `sequenceno` (
  `Id` int(10) NOT NULL,
  `StockTransactionNo` int(10) NOT NULL,
  `LendingNo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sequenceno`
--

INSERT INTO `sequenceno` (`Id`, `StockTransactionNo`, `LendingNo`) VALUES
(26, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `Id` int(10) NOT NULL,
  `ProductId` int(10) NOT NULL,
  `ProductCode` varchar(100) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `UnitId` int(10) NOT NULL,
  `UnitCode` varchar(100) NOT NULL,
  `UnitName` varchar(100) NOT NULL,
  `WarehouseId` int(10) NOT NULL,
  `WarehouseCode` varchar(100) NOT NULL,
  `WarehouseName` varchar(100) NOT NULL,
  `Quantity` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`Id`, `ProductId`, `ProductCode`, `ProductName`, `UnitId`, `UnitCode`, `UnitName`, `WarehouseId`, `WarehouseCode`, `WarehouseName`, `Quantity`) VALUES
(4, 4, '001', 'Product1', 2, 'lsn', 'lusin', 1, '001', 'Warehouse1', 10),
(5, 4, '001', 'Product1', 2, 'lsn', 'lusin', 34, '002', 'Warehouse2', 5);

-- --------------------------------------------------------

--
-- Table structure for table `stockmovement`
--

CREATE TABLE `stockmovement` (
  `Id` int(10) NOT NULL,
  `Date` date NOT NULL,
  `ProductId` int(10) NOT NULL,
  `ProductCode` varchar(100) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `UnitId` int(10) NOT NULL,
  `UnitCode` varchar(100) NOT NULL,
  `UnitName` varchar(100) NOT NULL,
  `WarehouseId` int(10) NOT NULL,
  `WarehouseCode` varchar(100) NOT NULL,
  `WarehouseName` varchar(100) NOT NULL,
  `Quantity` int(100) NOT NULL,
  `Type` enum('In','Out') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stocktransaction`
--

CREATE TABLE `stocktransaction` (
  `Id` int(10) NOT NULL,
  `StockTransactionNo` varchar(100) NOT NULL,
  `Date` date NOT NULL,
  `Type` enum('In','Out') NOT NULL,
  `Status` enum('Active','Cancelled') NOT NULL,
  `EmployeeId` int(10) NOT NULL,
  `EmployeeCode` varchar(100) NOT NULL,
  `EmployeeName` varchar(100) NOT NULL,
  `WarehouseId` int(10) NOT NULL,
  `WarehouseCode` varchar(100) NOT NULL,
  `WarehouseName` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `ProductId` int(10) NOT NULL,
  `ProductCode` varchar(100) NOT NULL,
  `ProductName` varchar(100) NOT NULL,
  `UnitId` int(10) NOT NULL,
  `UnitCode` varchar(100) NOT NULL,
  `UnitName` varchar(100) NOT NULL,
  `Quantity` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocktransaction`
--

INSERT INTO `stocktransaction` (`Id`, `StockTransactionNo`, `Date`, `Type`, `Status`, `EmployeeId`, `EmployeeCode`, `EmployeeName`, `WarehouseId`, `WarehouseCode`, `WarehouseName`, `Description`, `ProductId`, `ProductCode`, `ProductName`, `UnitId`, `UnitCode`, `UnitName`, `Quantity`) VALUES
(26, '000001\n', '2019-03-24', 'In', 'Active', 1, '001', 'Employee1', 1, 'Warehouse1', '001', '', 4, '001', 'Product1', 2, 'lsn', 'lusin', 10),
(27, '000002\n', '2019-03-24', 'In', 'Cancelled', 1, '001', 'Employee1', 1, 'Warehouse1', '001', '', 4, '001', 'Product1', 2, 'lsn', 'lusin', 5),
(28, '000003\n', '2019-03-24', 'Out', 'Cancelled', 1, '001', 'Employee1', 1, 'Warehouse1', '001', '', 4, '001', 'Product1', 2, 'lsn', 'lusin', 5),
(29, '000004\n', '2019-03-24', 'In', 'Active', 1, '001', 'Employee1', 34, 'Warehouse2', '002', '', 4, '001', 'Product1', 2, 'lsn', 'lusin', 5);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `Id` int(10) NOT NULL,
  `SupplierCode` varchar(100) NOT NULL,
  `SupplierName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `Id` int(10) NOT NULL,
  `UnitCode` varchar(100) NOT NULL,
  `UnitName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`Id`, `UnitCode`, `UnitName`) VALUES
(1, 'pcs', 'pcs'),
(2, 'lsn', 'lusin'),
(10, 'w', 'w');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','member') NOT NULL DEFAULT 'member',
  `divisi` varchar(100) NOT NULL,
  `loker` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nik`, `nama`, `alamat`, `telepon`, `username`, `password`, `level`, `divisi`, `loker`) VALUES
(1, '001', 'Admin', 'Admin', '1500-102', 'Admin', 'Admin', 'admin', 'Admin', 'Admin'),
(2, '002', 'Member', 'Member', '1500-102', 'Member', 'Member', 'member', 'Member', 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `Id` int(10) NOT NULL,
  `WarehouseCode` varchar(100) NOT NULL,
  `WarehouseName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`Id`, `WarehouseCode`, `WarehouseName`) VALUES
(1, '001', 'Warehouse1'),
(34, '002', 'Warehouse2'),
(37, '003', 'Warehouse3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `EmployeeCode` (`EmployeeCode`);

--
-- Indexes for table `lending`
--
ALTER TABLE `lending`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `LendingNo` (`LendingNo`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `ProductCode` (`ProductCode`) USING BTREE,
  ADD KEY `UnitId` (`UnitId`);

--
-- Indexes for table `sequenceno`
--
ALTER TABLE `sequenceno`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `stockmovement`
--
ALTER TABLE `stockmovement`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `stocktransaction`
--
ALTER TABLE `stocktransaction`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `StockTransactionNo` (`StockTransactionNo`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `SupplierCode` (`SupplierCode`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UnitCode` (`UnitCode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `WarehouseCode` (`WarehouseCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lending`
--
ALTER TABLE `lending`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sequenceno`
--
ALTER TABLE `sequenceno`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stockmovement`
--
ALTER TABLE `stockmovement`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocktransaction`
--
ALTER TABLE `stocktransaction`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`UnitId`) REFERENCES `unit` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
