<?php session_start();
include_once("../config.php");
$result = mysqli_query($koneksi, "SELECT * FROM users ORDER BY nik DESC");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';

?>
<?php
// include database connection file
include_once("../config.php");

// Check if form is submitted for user update, then redirect to homepage after update
if(isset($_POST['update']))
{ 
  $id = $_GET['id'];
  $collectionDate = $_POST['CollectionDate'];
  $returnDate = $_POST['ReturnDate'];
  $employeeId = $_POST['EmployeeId'];
  $borrowerId = $_POST['BorrowerId'];
  $productId = $_POST['ProductId'];
  $quantity = $_POST['Quantity'];
  $description = $_POST['Description'];

  $resultEmployeeById = mysqli_query($koneksi, "SELECT * FROM employee WHERE Id=$employeeId");
  $resultBororowerById = mysqli_query($koneksi, "SELECT * FROM employee WHERE Id=$borrowerId");
  $resultProductById = mysqli_query($koneksi, "SELECT * FROM product WHERE Id=$productId");

  $rowEmployee = mysqli_fetch_array($resultEmployeeById);
  $rowBorrower = mysqli_fetch_array($resultBororowerById);
  $rowProduct = mysqli_fetch_array($resultProductById);

  $resultUnitById = mysqli_query($koneksi, "SELECT * FROM unit WHERE Id=$rowProduct[UnitId]");
  $rowUnit = mysqli_fetch_array($resultUnitById);
    
  // update user data
  $result = mysqli_query($koneksi, "UPDATE lending SET 
    CollectionDate='$collectionDate',returnDate='$returnDate',
    EmployeeId='$employeeId',EmployeeCode='$rowEmployee[EmployeeCode]',EmployeeName='$rowEmployee[EmployeeName]',
    BorrowerId='$borrowerId',BorrowerCode='$rowBorrower[EmployeeCode]',BorrowerName='$rowBorrower[EmployeeName]',
    ProductId='$borrowerId',ProductCode='$rowProduct[ProductCode]',ProductName='$rowProduct[ProductName]',
    UnitId='$rowUnit[Id]',UnitCode='$rowUnit[UnitCode]',UnitName='$rowUnit[UnitName]',
    WHERE Id=$id");
  
  // Redirect to homepage to display updated user in list
  header("Location: lendings.php");
}
?>
<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];

$query = mysqli_query($koneksi, "SELECT * FROM lending WHERE Id=$id");
$result = mysqli_fetch_array($query);
$collectionDate_ = date_format(date_create($result['CollectionDate']),"m/d/Y");
$returnDate_ = date_format(date_create($result['ReturnDate']),"m/d/Y");

$resultEmployee = mysqli_query($koneksi, "SELECT * FROM employee");
$resultBorrower = mysqli_query($koneksi, "SELECT * FROM employee");
$resultProduct = mysqli_query($koneksi, "SELECT * FROM product");
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title">Add Employee</a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
								<td>Invoice No</td>
								<td><?php echo $result['LendingNo']; ?></td>
							</tr>
							<tr>
								<td>Date</td>
								<td><input id="theDateFrom" type="date" name="CollectionDate" required></td>
							</tr>
                            <tr>
								<td>Return Date</td>
								<td><input id="theDateTo" type="date" name="ReturnDate" required></td>
							</tr>
							<tr>
								<td>Employee</td>
								<td>
                                    <select name="EmployeeId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultEmployee))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" 
                                            <?php if($result['EmployeeId'] == $row['Id']): ?> selected="selected"<?php endif; ?> > 
                                            <?php echo($row['EmployeeName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Borrower</td>
								<td>
                                    <select name="BorrowerId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultBorrower))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>"
                                        <?php if($result['BorrowerId'] == $row['Id']): ?> selected="selected"<?php endif; ?> > 
                                            <?php echo($row['EmployeeName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Product</td>
								<td>
                                    <select name="ProductId" class="browser-default" required>
										<?php
										while($row = mysqli_fetch_array($resultProduct))
										{?>
										<option value = "<?php echo($row['Id'])?>" 
                                        <?php if($result['ProductId'] == $row['Id']): ?> selected="selected"<?php endif; ?> > 
											<?php echo($row['ProductName']); ?>
										</option>
										<?php }?>
									</select>
                                </td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td><input type="number" value="<?php echo $result['Quantity']; ?>" name="Quantity" required></td>
							</tr>
                            <tr>
								<td>Keterangan</td>
								<td><input type="text" value="<?php echo $result['Description']; ?>" name="Description"></td>
							</tr>
                        </table>
                        <table>
				            <tr>
				            	<th>
				            		<input type="submit" name="update" value="Edit" class="right waves-effect waves-light btn green darken-2" style="float: left;">
				            	</th>
				            	<th style="width: 1%;">
				            		<a href="lendings.php"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
				            	</th>
				            </tr>
				        </table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		});
        var date = new Date();

        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var today = year + "-" + month + "-" + day;  
        (document).getElementById("theDateFrom").value = today;
        (document).getElementById("theDateTo").value = today;
	</script>
</body>
</html>