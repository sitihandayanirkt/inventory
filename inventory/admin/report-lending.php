<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
require('../assets/pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'PT. ABCD',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. ABCD',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.abcd.com email :abcd@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Pinjam Barang",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Tgl Ambil', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Tgl Kembali', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Petugas', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Peminjam', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Barang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Quantity', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Status', 1, 1, 'C');
$pdf->SetFont('Arial','',10);

$cari = $_GET['cari'];
$start = $_GET['start'];
$per_hal = $_GET['per_hal'];
$dateFrom = $_GET['dateFrom'];
$dateTo = $_GET['dateTo'];
$no=1;
if($cari != null && $cari != "")
	$sql = "SELECT * FROM lending WHERE LendingNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR BorrowerCode LIKE '%$cari%' OR BorrowerName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%'
	AND CollectionDate >= '$dateFrom' AND CollectionDate <= '$dateTo'";
else
	$sql = "SELECT * FROM lending WHERE CollectionDate > '$dateFrom' AND CollectionDate < '$dateTo'";

$query=mysqli_query($koneksi, $sql);
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['CollectionDate'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['ReturnDate'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['EmployeeName'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['BorrowerName'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['ProductName'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['Quantity']." ".$lihat['UnitName'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['Status'], 1, 1,'C');

	$no++;
}

$pdf->Output("laporan_stock.pdf","I");

?>

