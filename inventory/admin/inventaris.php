<?php session_start();
include_once("../config.php");

$type = 1;
$string = "In";
$title = "Barang Masuk";
if($_GET['type'] == 'Out')
{
	$type = 2;
	$string = "Out";
	$title = "Permintaan ATK";
}

$result = mysqli_query($koneksi, "SELECT * FROM stocktransaction WHERE Type = $type");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$valueCari = '';

if(isset($_POST['cari']))
{
	$cari = $_POST['cari'];
	$valueCari = $cari;
	$pnjng = 100;
	$substr = substr($cari, 0, $pnjng).' ...';
	if($cari != null && $cari != '')
		$sql = "SELECT * FROM stocktransaction WHERE StockTransactionNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%'
				AND Type = $type";
	else
		$sql = "SELECT * FROM stocktransaction WHERE Type = $type";

	$result = mysqli_query($koneksi,$sql);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title"><?php echo $title; ?></a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l12 offset-l2"> <br>
					<!--kolom search-->
					<div class="col s12 m12 l12">
						<form method="post" action="stocktransactions.php?type=<?php echo $_GET['type']?>" class="row">
							<div class="e-input-field col s12 m12 l12">
								<input value="<?php echo $valueCari ?>" name="cari" onchange="this.form.submit();" type="text" name="cari" placeholder="Cari Berdasarkan Employee Code / Nama" class="validate" required title="Cari User">
								<input type="submit" name="cari2" value="cari" class="btn right red darken-2"> 
							</div>
						</form>
					</div>

					<!--table-->
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom header table-->
							<tr>
			          			<th hidden>ID</th>
								<th>Kode</th>
								<th>Tanggal</th>
                                <th>Employee Code</th>
                                <th>Employee Name</th>
                                <th>Warehouse Code</th>
                                <th>Warehouse Name</th>
								<th>Product Code</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Status</th>
				      </tr>

							<?php 

							while($user_data = mysqli_fetch_array($result)) 
							{ 
									echo "<tr>";
									echo "<td hidden>".$user_data['Id']."</td>";
                                    echo "<td> <a href='detail-stocktransaction.php?id=$user_data[Id]' style='text-decoration: none;'><h6>".$user_data['StockTransactionNo']."</h6></a></td>";
									echo "<td> <h6>".$user_data['Date']."</h6> </td>";
									echo "<td> <h6>".$user_data['EmployeeCode']."</h6></td>";
									echo "<td> <h6>".$user_data['EmployeeName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['WarehouseCode']."</h6></td>";
									echo "<td> <h6>".$user_data['WarehouseName']."</h6> </td>";
									echo "<td> <h6>".$user_data['ProductCode']."</h6></td>";
									echo "<td> <h6>".$user_data['ProductName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['Quantity']."</h6></td>";
                                    echo "<td> <h6>".$user_data['Status']."</h6> </td>";
									echo "</tr>";
							}

							?>
							
						</table>
						<table>
							<tr>
								<td colspan='9'>
									<a href='add-stocktransaction.php?type=<?php echo $_GET['type']?>' class="right waves-effect waves-light btn red darken-2">Add Stock <?php echo $string; ?><i class="material-icons right">add</i></a>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</main>
        <!--end of content-->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
			});
	</script>
</body>
</html>