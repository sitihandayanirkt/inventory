<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';

$type = 1;
$typeString = "In";
$string = "In";
$title = "Tambah Barang Masuk";
if($_GET['type'] == 'Out')
{
	$type = 0;
	$typeString = "Out";
	$string = "Out";
	$title = "Tambah Pemintaan ATK";
}

$resultWarehouse = mysqli_query($koneksi, "SELECT * FROM warehouse");
$resultEmployee = mysqli_query($koneksi, "SELECT * FROM employee");

$resultProduct = mysqli_query($koneksi, "SELECT * FROM product");
$resultProductStock = mysqli_query($koneksi, 
				"SELECT p.Id, p.ProductCode, p.ProductName, p.UnitId, p.UnitCode, p.UnitName, SUM(s.Quantity) as Quantity
				FROM product p
				LEFT JOIN stock s
				ON p.Id = s.ProductId
                GROUP BY p.Id, p.ProductCode, p.ProductName,
                		p.UnitId, p.UnitCode, p.UnitName");

$resultSequenceNo = mysqli_query($koneksi, "SELECT * FROM sequenceno ORDER BY Id DESC LIMIT 1");
$rowLastNumber = mysqli_fetch_array($resultSequenceNo);
$lastNumber = 1;
if($rowLastNumber['Id'] != null)
	$lastNumber = $rowLastNumber['StockTransactionNo'] + 1;

$noInvoice = sprintf("%'.06d\n", $lastNumber);

$disableButton = false;

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title"><?php echo $title; ?></a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
								<td>Invoice No</td>
								<td><input type="text" readonly name="StockTransactionNo" value="<?php echo $noInvoice ?>" required></td>
							</tr>
							<tr>
								<td>Date</td>
								<td><input type="date" name="Date" required></td>
							</tr>
							<tr>
								<td>Employee</td>
								<td>
                                    <select name="EmployeeId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultEmployee))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['EmployeeName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Warehouse</td>
								<td>
                                    <select name="WarehouseId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultWarehouse))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['WarehouseName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
							<tr>
								<td>Product</td>
								<td>
                                    <select name="ProductId" class="browser-default" required>
										<?php
										while($row = mysqli_fetch_array($resultProductStock))
										{?>
										<option value = "<?php echo($row['Id'])?>" >
											<?php 
												echo($row['ProductName'] . "   => Stock: " . $row['Quantity']);
											 ?>
										</option>
										<?php }?>
									</select>
                                </td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td><input type="number" name="Quantity" required></td>
							</tr>
                            <tr>
								<td>Keterangan</td>
								<td><input type="text" name="Description"></td>
							</tr>
						</table>
						<table>
							<tr>
								<th>
									<input type="submit" name="tambah" value="Simpan" class="right waves-effect waves-light btn green darken-2" style="float: left;">
								</th>
								<th style="width: 1%;">
									<a href="stocktransactions.php?type=In"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
								</th>
							</tr>
				    </table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

        <!-- Proses Penambahan Data User -->

        <?php
          // Check If form submitted, insert form data into users table.
          if(isset($_POST['tambah'])) {
			$disableButton = true;

            $date = $_POST['Date'];
			$employeeId = $_POST['EmployeeId'];
			$warehouseId = $_POST['WarehouseId'];
			$productId = $_POST['ProductId'];
			$quantity = $_POST['Quantity'];
			$description = $_POST['Description'];
			$status = "Active";

			$resultWarehouseById = mysqli_query($koneksi, "SELECT * FROM warehouse WHERE Id=$warehouseId");
			$resultEmployeeById = mysqli_query($koneksi, "SELECT * FROM employee WHERE Id=$employeeId");
			$resultProductById = mysqli_query($koneksi, "SELECT * FROM product WHERE Id=$productId");
			
			$rowWarehouse = mysqli_fetch_array($resultWarehouseById);
			$rowEmployee = mysqli_fetch_array($resultEmployeeById);
			$rowProduct = mysqli_fetch_array($resultProductById);

			$resultUnitById = mysqli_query($koneksi, "SELECT * FROM unit WHERE Id=$rowProduct[UnitId]");
			$rowUnit = mysqli_fetch_array($resultUnitById);

			$resultStockById = mysqli_query($koneksi, "SELECT * FROM stock WHERE WarehouseId=$warehouseId && ProductId=$productId");
			$rowStock = mysqli_fetch_array($resultStockById);
            
            // include database connection file
			include_once("../config.php");
                
            // Insert user data into table
            $resultInsertStockTransaction = mysqli_query($koneksi, "INSERT INTO stocktransaction
					(StockTransactionNo,Date,Type,Status,
					EmployeeId,EmployeeCode,EmployeeName,
					WarehouseId,WarehouseName,WarehouseCode,Description,
					ProductId,ProductCode,ProductName,
					UnitId,UnitCode,UnitName,Quantity) 
					VALUES('$noInvoice','$date','$typeString','$status',
					'$employeeId','$rowEmployee[EmployeeCode]','$rowEmployee[EmployeeName]',
					'$warehouseId','$rowWarehouse[WarehouseName]','$rowWarehouse[WarehouseCode]','$description',
					'$productId','$rowProduct[ProductCode]','$rowProduct[ProductName]',
					'$rowUnit[Id]','$rowUnit[UnitCode]','$rowUnit[UnitName]','$quantity')");

			if($rowLastNumber['Id'] != null)
			{
				$resultInsertSequenceNo = mysqli_query($koneksi, "UPDATE sequenceno SET StockTransactionNo = $lastNumber WHERE Id=$rowLastNumber[Id]");
			}
			else
			{
				$resultInsertSequenceNo = mysqli_query($koneksi, "INSERT INTO sequenceno (StockTransactionNo,LendingNo) VALUES ($lastNumber,0)");
			}

			// Insert user data into table
			if($rowStock['Id'] == null)
			{
				if($type == 1)
				{
					$resultInsertStock = mysqli_query($koneksi, "INSERT INTO stock
							(ProductId,ProductCode,ProductName,
							UnitId,UnitCode,UnitName,
							WarehouseId,WarehouseName,WarehouseCode,Quantity) 
							VALUES('$productId','$rowProduct[ProductCode]','$rowProduct[ProductName]',
							'$rowUnit[Id]','$rowUnit[UnitCode]','$rowUnit[UnitName]',
							'$warehouseId','$rowWarehouse[WarehouseName]','$rowWarehouse[WarehouseCode]','$quantity')");
				}
				else
				{
					echo "<script>alert('Stock Tidak Ada')</script>";
				}
			}
			else
			{
				if($type == 1)
				{
					$newQuantity = $rowStock['Quantity'] + $quantity;
					$resultUpdateStock = mysqli_query($koneksi, "UPDATE stock SET Quantity = $newQuantity WHERE Id=$rowStock[Id]");
				}
				else
				{
					$newQuantity = $rowStock['Quantity'] - $quantity;
					if($newQuantity >= 0)
					{
						$resultUpdateStock = mysqli_query($koneksi, "UPDATE stock SET Quantity = $newQuantity WHERE Id=$rowStock[Id]");
						$result = mysqli_query($koneksi, "UPDATE stocktransaction SET Status = 1 WHERE Id=$id");
					}
					else
					{
						echo "<script>alert('Stock Tidak Cukup')</script>";
					}
				}
			}
			
			$_POST = array();
            echo "<script>alert('Tambah Stock Berhasil'); window.location.href='stocktransactions.php?type=In';</script>";
			
          }
        ?>

        <!-- End -->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		})
	</script>
	<!-- <script>
        $(".browser-default").click(function () {
			<?php
				$productId_ = $_POST['ProductId'];
				$resultStockById_ = mysqli_query($koneksi, "SELECT SUM(s.Quantity) as Quantity FROM stock s WHERE s.ProductId = $productId_");
				$rowStock_ = mysqli_fetch_array($resultStockById_);
			?>
			$( ".stockQty" ).text( "Stock: 111" );
        });
    </script> -->
</body>
</html>