<!--Sidenav-->
<ul id="slide-out" class="side-nav fixed">
        
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <div class="user-view">
                    <div class="background" style="margin-bottom:-15%;">
                        <img src="../images/bg.jpg">
                    </div>
                    <span class="white-text name"><?php echo $nama; ?><i class="material-icons left">account_circle</i></span>
                </div>
            </li>
            
            <li><div class="divider" style="margin-top:15%;"></div></li>

            <li><a href="index.php" class="collapsible-header">Beranda<i class="material-icons">home</i></a></li>

            <li>
                <a class="collapsible-header">Menu<i class="material-icons">arrow_drop_down</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="user.php">User</a></li>
                        <li><a href="employees.php">Employee</a></li>
                        <li><a href="units.php">Unit</a></li>
                        <li><a href="products.php">Product</a></li>
                        <li><a href="warehouses.php">Warehouse</a></li>
                        <li><a href="stocktransactions.php?type=In">Stock IN</a></li>
                        <li><a href="stocktransactions.php?type=Out">Stock Out</a></li>
                        <li><a href="stock.php">Stock</a></li>
                        <li><a href="lendings.php">Inventaris</a></li>
                        <li><a href="barangkeluar.php">Laporan</a></li>
                    </ul>
                </div>
            </li>

            <li><a href="../logout.php" class="collapsible-header">Keluar<i class="material-icons">exit_to_app</i></a></li>

        </ul>
    </li>

</ul>