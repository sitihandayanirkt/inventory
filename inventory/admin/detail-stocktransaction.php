<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$result = mysqli_query($koneksi, "SELECT * FROM stocktransaction WHERE Id = $_GET[id]");
$user_data = mysqli_fetch_array($result);
$title = "Detail Barang Masuk";
if($user_data['Type'] == 'Out')
{
	$title = "Detail Permintaan ATK";
}
$disableButton = false;

?>
<!DOCTYPE html>
<html>
<head>
	<?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
            <!--TopNav-->
            <nav class="row top-nav red darken-2">
                    <div class="container">
                            <div class="col offset-l2 nav-wrapper">
                                    <a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
                                    <a class="page-title"><?php echo $title; ?></a>
                            </div>
                    </div>
            </nav>
            <?php include 'sidenav.php';?>
        </header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l12 offset-l2"> <br>

					<!--table-->
					<form action="" method="post" name="form1">
					    <div class="col s12 m12 l12 card-panel z-depth"> <br>
                            <table class="highlight">
                                <!--kolom header table-->
                                <tr>
                                    <td>Kode</td><td><?php echo $user_data['StockTransactionNo'] ?></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td><td><?php echo $user_data['Date'] ?></td>
                                </tr>
                                <tr>
                                    <td>Status</td><td><?php echo $user_data['Status'] ?></td>
                                </tr>
                                <tr>
                                    <td>Employee Code</td><td><?php echo $user_data['EmployeeCode'] ?></td>
                                </tr>
                                <tr>
                                    <td>Employee Name</td><td><?php echo $user_data['EmployeeName'] ?></td>
                                </tr>
                                <tr>
                                    <td>Warehouse Code</td><td><?php echo $user_data['WarehouseCode'] ?></td>
                                </tr>
                                <tr>
                                    <td>Warehouse Name</td><td><?php echo $user_data['WarehouseName'] ?></td>
                                </tr>
                                <tr>
                                    <td>Product Code</td><td><?php echo $user_data['ProductCode'] ?></td>
                                </tr>
                                <tr>
                                    <td>Product Name</td><td><?php echo $user_data['ProductName'] ?></td>
                                </tr>
                                <tr>
                                    <td>Quantity</td><td><?php echo $user_data['Quantity'] ?></td>
                                </tr>
                                
                            </table>
                            <table>
                                <tr>
                                    <td colspan='9'>
                                    <input type="button" id="cancel" name="cancel" value="Cancel" class="right waves-effect waves-light btn red darken-2" style="float: left;">
                                    </td>
                                </tr>
                            </table>
					    </div>
                    </form>
				</div>
			</div>
		</main>
        <!--end of content-->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
			});
	</script>
	<script>
        $("#cancel").click(function () {
        var jawab = confirm("Anda Yakin Ingin Cancel Stock Transaction ?");
        if (jawab === true) {
        // konfirmasi
            var hapus = false;
            if (!hapus) {
                hapus = true;
                $.post('cancel-stocktransaction.php?id=<?php echo $user_data["Id"]; ?>', {id: <?php echo $user_data['Id']; ?>},
                function (data) {
                    // alert(data);
                });
                hapus = false;
            }
        } else {
            return false;
        }
        });
      </script>
</body>
</html>