<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';

$resultWarehouse = mysqli_query($koneksi, "SELECT * FROM warehouse");
$resultEmployee = mysqli_query($koneksi, "SELECT * FROM employee");
$resultBorrower = mysqli_query($koneksi, "SELECT * FROM employee");

$resultProduct = mysqli_query($koneksi, "SELECT * FROM product WHERE Id NOT IN (SELECT DISTINCT ProductId FROM lending WHERE Status = 1)");

$resultSequenceNo = mysqli_query($koneksi, "SELECT * FROM sequenceno ORDER BY Id DESC LIMIT 1");
$rowLastNumber = mysqli_fetch_array($resultSequenceNo);
$lastNumber = 1;
if($rowLastNumber['Id'] != null)
	$lastNumber = $rowLastNumber['LendingNo'] + 1;

$noInvoice = sprintf("%'.06d\n", $lastNumber);

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'headmenu.php';?>
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title">Add Employee</a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l10 offset-l3"> <br>

					<!--table-->
				<form action="" method="post" name="form1">
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom isian table-->
							<tr>
								<td>Invoice No</td>
								<td><?php echo $noInvoice ?></td>
							</tr>
							<tr>
								<td>Date</td>
								<td><input type="date" name="CollectionDate" required></td>
							</tr>
                            <tr>
								<td>Return Date</td>
								<td><input type="date" name="ReturnDate" required></td>
							</tr>
							<tr>
								<td>Employee</td>
								<td>
                                    <select name="EmployeeId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultEmployee))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['EmployeeName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Borrower</td>
								<td>
                                    <select name="BorrowerId" class="browser-default" required>
                                        <?php
                                        while($row = mysqli_fetch_array($resultBorrower))
                                        {?>
                                        <option value = "<?php echo($row['Id'])?>" >
                                            <?php echo($row['EmployeeName']) ?>
                                        </option>
                                        <?php }?>
                                    </select>
                                </td>
							</tr>
                            <tr>
								<td>Product</td>
								<td>
                                    <select name="ProductId" class="browser-default" required>
										<?php
										while($row = mysqli_fetch_array($resultProduct))
										{?>
										<option value = "<?php echo($row['Id'])?>" >
											<?php 
												echo($row['ProductName']);
											 ?>
										</option>
										<?php }?>
									</select>
                                </td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td><input type="number" name="Quantity" required></td>
							</tr>
                            <tr>
								<td>Keterangan</td>
								<td><input type="text" name="Description"></td>
							</tr>
						</table>
						<table>
							<tr>
								<th>
									<input type="submit" name="tambah" value="Tambah" class="right waves-effect waves-light btn green darken-2" style="float: left;">
								</th>
								<th style="width: 1%;">
									<a href="lendings.php"><input type="button" value="Kembali" class="right waves-effect waves-light btn red darken-2"></a> 
								</th>
							</tr>
				        </table>
					</div>
				</form>
				</div>
			</div>
		</main>
        <!--end of content-->

        <!-- Proses Penambahan Data User -->

        <?php
 
          // Check If form submitted, insert form data into users table.
          if(isset($_POST['tambah'])) {
            $collectionDate = $_POST['CollectionDate'];
            $returnDate = $_POST['ReturnDate'];
			$employeeId = $_POST['EmployeeId'];
			$borrowerId = $_POST['BorrowerId'];
			$productId = $_POST['ProductId'];
			$quantity = $_POST['Quantity'];
			$description = $_POST['Description'];
            $status = "Active";
            
            $resultEmployeeById = mysqli_query($koneksi, "SELECT * FROM employee WHERE Id=$employeeId");
            $resultBororowerById = mysqli_query($koneksi, "SELECT * FROM employee WHERE Id=$borrowerId");
			$resultProductById = mysqli_query($koneksi, "SELECT * FROM product WHERE Id=$productId");
            
			$rowEmployee = mysqli_fetch_array($resultEmployeeById);
			$rowBororower = mysqli_fetch_array($resultBororowerById);
			$rowProduct = mysqli_fetch_array($resultProductById);

			$resultUnitById = mysqli_query($koneksi, "SELECT * FROM unit WHERE Id=$rowProduct[UnitId]");
			$rowUnit = mysqli_fetch_array($resultUnitById);
            
            // include database connection file
            include_once("../config.php");
                
            // Insert user data into table
            $result = mysqli_query($koneksi, "INSERT INTO lending(
                    LendingNo,CollectionDate,ReturnDate,
                    EmployeeId,EmployeeCode,EmployeeName,
                    BorrowerId,BorrowerCode,BorrowerName,
                    ProductId,ProductCode,ProductName,
                    UnitId,UnitCode,UnitName,
                    Quantity,Status,Description) 
                    VALUES('$noInvoice','$collectionDate','$returnDate',
                    '$employeeId','$rowEmployee[EmployeeCode]','$rowEmployee[EmployeeName]',
                    '$borrowerId','$rowBororower[EmployeeCode]','$rowBororower[EmployeeName]',
                    '$productId','$rowProduct[ProductCode]','$rowProduct[ProductName]',
                    '$rowUnit[Id]','$rowUnit[UnitCode]','$rowUnit[UnitName]',
                    '$quantity','$status','$description')");
            
            if($rowLastNumber['Id'] != null)
			{
				$resultInsertSequenceNo = mysqli_query($koneksi, "UPDATE sequenceno SET LendingNo = $lastNumber WHERE Id=$rowLastNumber[Id]");
			}
			else
			{
				$resultInsertSequenceNo = mysqli_query($koneksi, "INSERT INTO sequenceno (StockTransactionNo,LendingNo) VALUES (0,$lastNumber)");
            }
            
            echo "<script>alert('Peminjaman Berhasil'); window.location.href='lendings.php';</script>";
          }
        ?>

        <!-- End -->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
		});
	</script>
</body>
</html>