<?php
private $pdo = null;
public function transfer($from, $to, $amount) {
 
    try {
        $this->pdo->beginTransaction();

        // Insert user data into table
        $resultInsertStockTransaction = mysqli_query($koneksi, "INSERT INTO stocktransaction
                (StockTransactionNo,Date,Type,Status,
                EmployeeId,EmployeeCode,EmployeeName,
                WarehouseId,WarehouseName,WarehouseCode,Description,
                ProductId,ProductCode,ProductName,
                UnitId,UnitCode,UnitName,Quantity) 
                VALUES('$noInvoice','$date','$type','$status',
                '$employeeId','$rowEmployee[EmployeeCode]','$rowEmployee[EmployeeName]',
                '$warehouseId','$rowWarehouse[WarehouseCode]','$rowWarehouse[WarehouseName]','$description',
                '$productId','$rowProduct[ProductCode]','$rowProduct[ProductName]',
                '$rowUnit[Id]','$rowUnit[UnitCode]','$rowUnit[UnitName]','$quantity')");
        $stmt = $this->pdo->prepare($resultInsertStockTransaction);
        $stmt->execute();
        $stmt->closeCursor();

        // deduct from the transferred account
        $resultInsertSequenceNo = mysqli_query($koneksi, "INSERT INTO sequenceno (Id) VALUES (NULL)");
        $stmt = $this->pdo->prepare($resultInsertSequenceNo);
        $stmt->execute();
        $stmt->closeCursor();

        // add to the receiving account
        if($rowStock == null)
        {
            $resultInsertStock = mysqli_query($koneksi, "INSERT INTO stock
                    (ProductId,ProductCode,ProductName,
                    UnitId,UnitCode,UnitName,
                    WarehouseId,WarehouseName,WarehouseCode,Quantity) 
                    VALUES('$productId','$rowProduct[ProductCode]','$rowProduct[ProductName]',
                    '$rowUnit[Id]','$rowUnit[UnitCode]','$rowUnit[UnitName]',
                    '$warehouseId','$rowWarehouse[WarehouseName]','$rowWarehouse[WarehouseCode]','$quantity')");
            $stmt = $this->pdo->prepare($resultInsertStock);
            $stmt->execute();
        }
        else
        {
            $newQuantity = $rowStock['Quantity'] + $quantity;
            $resultUpdateStock = mysqli_query($koneksi, "UPDATE stock SET Quantity = $newQuantity WHERE Id=$rowStock[Id]");
            $stmt = $this->pdo->prepare($resultUpdateStock);
            $stmt->execute();
        }

        // commit the transaction
        $this->pdo->commit();

        echo "<script>alert('Tambah Stock Berhasil')</script>";
        // header("Location: add-stocktransaction.php");

        return true;
    } catch (PDOException $e) {
        $this->pdo->rollBack();
        die($e->getMessage());
    }
}
?>