<?php
// include database connection file
include_once("../config.php");
 
// Get id from URL to delete that user
$id = $_GET['id'];

$resultStockTransactionById = mysqli_query($koneksi, "SELECT * FROM stocktransaction WHERE Id=$id");
$rowStockTransaction = mysqli_fetch_array($resultStockTransactionById);
if($rowStockTransaction['Id'] != null)
{
    $resultStockById = mysqli_query($koneksi, "SELECT * FROM stock WHERE WarehouseId=$rowStockTransaction[WarehouseId] AND ProductId=$rowStockTransaction[ProductId]");
    $rowStock = mysqli_fetch_array($resultStockById);
    
    if($rowStock['Id'] != null)
    {
        if($rowStockTransaction['Type'] == 'In')
        {
            $newQuantity = $rowStock['Quantity'] - $rowStockTransaction['Quantity'];
            if($newQuantity >= 0)
            {
                $resultUpdateStock = mysqli_query($koneksi, "UPDATE stock SET Quantity = $newQuantity WHERE Id=$rowStock[Id]");
            }
            else
            {
                echo "<script>alert('Stock Tidak Cukup')</script>";
            }
        }
        else
        {
            $newQuantity = $rowStock['Quantity'] + $rowStockTransaction['Quantity'];
            $resultUpdateStock = mysqli_query($koneksi, "UPDATE stock SET Quantity = $newQuantity WHERE Id=$rowStock[Id]");
        }

        $result = mysqli_query($koneksi, "UPDATE stocktransaction SET Status = 2 WHERE Id=$id");
    }
    else
    {
        echo "<script>alert('Stock Tidak Ada')</script>";
    }
}
 
// After delete redirect to Home, so that latest user list will be displayed.
header("Location: products.php");
?>
