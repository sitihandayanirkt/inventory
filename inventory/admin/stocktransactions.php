<?php session_start();
include_once("../config.php");

$valueCari = '';
$_dateFrom = "";
$_dateTo = "";
$cari = "";
$type = 1;
$string = "In";
$title = "Barang Masuk";
if($_GET['type'] == 'Out')
{
	$type = 2;
	$string = "Out";
	$title = "Permintaan ATK";
}

$dateFrom = date("Y-m-01");
$dateTo = date("Y-m-t");
$per_hal=10;
$jum=1;

if(isset($_GET['dateFrom']))
	$dateFrom = $_GET['dateFrom'];

if(isset($_GET['dateTo']))
	$dateTo = $_GET['dateTo'];

if(isset($_GET['cari']) && $_GET['cari'] != null && $_GET['cari'] != "")
{
	$cari =  $_GET['cari'];
	$valueCari =  $_GET['cari'];
	$sql_record = "SELECT COUNT(*) AS Count FROM stocktransaction WHERE (StockTransactionNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%')
					AND Type = $type
					AND Date >= '$dateFrom' AND Date <= '$dateTo'";
}
else
{
	$sql_record = "SELECT COUNT(*) AS Count FROM stocktransaction WHERE Type = $type AND Date >= '$dateFrom' AND Date <= '$dateTo'";
}

$jumlah_record=mysqli_query($koneksi, $sql_record);
while($data = mysqli_fetch_array($jumlah_record)) 
{ 
	if($data['Count'] > 0)
		$jum = $data['Count'];
}
$halaman=ceil($jum / $per_hal);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $per_hal;

$result = mysqli_query($koneksi, "SELECT * FROM stocktransaction WHERE Type = $type AND Date >= '$dateFrom' AND Date <= '$dateTo'
									 limit $start, $per_hal");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';

if(isset($_POST['cari']) || (isset($_POST['dateFrom']) && isset($_POST['dateTo'])))
{
	$cari = $_POST['cari'];
	$valueCari = $cari;
	$dateFrom = $_POST['dateFrom'];
	$dateTo = $_POST['dateTo'];
	$date1=date_create($dateFrom);
	$date2=date_create($dateTo);
	$_dateFrom = date_format($date1,"Y-m-d");
	$_dateTo =  date_format($date2,"Y-m-d");
	$pnjng = 100;
	$substr = substr($cari, 0, $pnjng).' ...';

	if($cari != null && $cari != '')
	{
		$sql = "SELECT * FROM stocktransaction WHERE (StockTransactionNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%')
				AND Type = $type
				AND Date >= '$_dateFrom' AND Date <= '$_dateTo'
				limit $start, $per_hal";

		$sql_record2 = "SELECT COUNT(*) AS Count FROM stocktransaction WHERE (StockTransactionNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%')
				AND Type = $type
				AND Date >= '$_dateFrom' AND Date <= '$_dateTo'";
	}
	else
	{
		$sql = "SELECT * FROM stocktransaction WHERE Type = $type
				AND Date >= '$_dateFrom' AND Date <= '$_dateTo'
				limit $start, $per_hal";

		$sql_record2 = "SELECT COUNT(*) AS Count FROM stocktransaction WHERE Type = $type
				AND Date >= '$_dateFrom' AND Date <= '$_dateTo'";
	}

	$jumlah_record2=mysqli_query($koneksi, $sql_record2);
	while($data = mysqli_fetch_array($jumlah_record2)) 
	{ 
		if($data['Count'] > 0)
			$jum = $data['Count'];
	}
	$halaman=ceil($jum / $per_hal);
	$page = 1;
	$start = 1;

	$result = mysqli_query($koneksi,$sql);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title"><?php echo $title; ?></a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l12 offset-l2"> <br>
					<form method="post" action="stocktransactions.php?type=<?php echo $_GET['type']?>" class="row">
						<div class="col s3 m3 l3">
							<input type="date" name="dateFrom" value="<?php echo $dateFrom ?>">
						</div>
						<div class="col s3 m3 l3">
							<input type="date" name="dateTo" value="<?php echo $dateTo ?>">
						</div>
						<!--kolom search-->
						<div class="col s6 m6 l6">
							<div class="e-input-field col s12 m12 l12">
								<input value="<?php echo $valueCari ?>" name="cari" type="text" name="cari" placeholder="Cari Berdasarkan Employee Code / Nama" class="validate" title="Cari User">
								<input type="submit" name="cari2" value="cari" class="btn right red darken-2"> 
							</div>
						</div>
					</form>

					<a  style="margin-bottom:10px" href="report-stocktransaction.php?cari=<?php echo $cari ?>&start=<?php echo $start ?>&per_hal=<?php echo $per_hal ?>&dateFrom=<?php echo $dateFrom ?>&dateTo=<?php echo $dateTo ?>&type=<?php echo $type ?>" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
					<!--table-->
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom header table-->
							<tr>
			          			<th hidden>ID</th>
								<th>Kode</th>
								<th>Tanggal</th>
                                <th>Employee Code</th>
                                <th>Employee Name</th>
                                <th>Warehouse Code</th>
                                <th>Warehouse Name</th>
								<th>Product Code</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Status</th>
				      </tr>

							<?php 

							while($user_data = mysqli_fetch_array($result)) 
							{ 
									echo "<tr>";
									echo "<td hidden>".$user_data['Id']."</td>";
                                    echo "<td> <a href='detail-stocktransaction.php?id=$user_data[Id]' style='text-decoration: none;'><h6>".$user_data['StockTransactionNo']."</h6></a></td>";
									echo "<td> <h6>".$user_data['Date']."</h6> </td>";
									echo "<td> <h6>".$user_data['EmployeeCode']."</h6></td>";
									echo "<td> <h6>".$user_data['EmployeeName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['WarehouseCode']."</h6></td>";
									echo "<td> <h6>".$user_data['WarehouseName']."</h6> </td>";
									echo "<td> <h6>".$user_data['ProductCode']."</h6></td>";
									echo "<td> <h6>".$user_data['ProductName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['Quantity']." ".$user_data['UnitName']."</h6></td>";
                                    echo "<td> <h6>".$user_data['Status']."</h6> </td>";
									echo "</tr>";
							}

							?>
							
						</table>
						<ul class="pagination">			
							<?php 
							for($x=1;$x<=$halaman;$x++){
								?>
								<li><a href="?type=<?php echo $_GET['type']?>&cari=<?php echo $cari?>&dateFrom=<?php echo $dateFrom ?>&dateTo=<?php echo $dateTo ?>&page=<?php echo $x ?>"><?php echo $x ?></a></li>
								<?php
							}
							?>						
						</ul>
						<table>
							<tr>
								<td colspan='9'>
									<a href='add-stocktransaction.php?type=<?php echo $_GET['type']?>' class="right waves-effect waves-light btn red darken-2">Add Stock <?php echo $string; ?><i class="material-icons right">add</i></a>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</main>
        <!--end of content-->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
			});
	</script>
</body>
</html>