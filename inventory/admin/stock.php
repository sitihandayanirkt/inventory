<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$valueCari = '';

$per_hal=10;
$jum=0;
$jumlah_record=mysqli_query($koneksi, "SELECT COUNT(*) AS Count FROM stock");
while($data = mysqli_fetch_array($jumlah_record)) 
{ 
	$jum = $data['Count'];
}
$halaman=ceil($jum / $per_hal);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $per_hal;

$result = mysqli_query($koneksi, "SELECT * FROM stock limit $start, $per_hal");

$cari="";
if(isset($_POST['cari']))
{
	$cari = $_POST['cari'];
	$valueCari = $cari;
	$pnjng = 100;
	$substr = substr($cari, 0, $pnjng).' ...';
	if($cari != null && $cari != '')
	{
		$sql = "SELECT * FROM stock WHERE ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%'
				limit $start, $per_hal";

		$sql_record2 = "SELECT COUNT(*) AS Count FROM stock WHERE ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%'";
	}
	else
	{
		$sql = "SELECT * FROM stock limit $start, $per_hal";

		$sql_record2 = "SELECT COUNT(*) AS Count FROM stock";
	}

	$jumlah_record2=mysqli_query($koneksi, $sql_record2);
	while($data = mysqli_fetch_array($jumlah_record2)) 
	{ 
		if($data['Count'] > 0)
			$jum = $data['Count'];
	}
	$halaman=ceil($jum / $per_hal);
	$page = 1;
	$start = 1;

	$result = mysqli_query($koneksi,$sql);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title">Stock</a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l12 offset-l2"> <br>
					<!--kolom search-->
					<div class="col s12 m12 l12">
						<form method="post" action="stock.php" class="row">
							<div class="e-input-field col s12 m12 l12">
								<input value="<?php echo $valueCari ?>" name="cari" onchange="this.form.submit();" type="text" name="cari" placeholder="Cari Berdasarkan Employee Code / Nama" class="validate" required title="Cari User">
								<input type="submit" name="cari2" value="cari" class="btn right red darken-2"> 
							</div>
						</form>
					</div>

					<a style="margin-bottom:10px" href="report-stock.php?cari=<?php echo $cari ?>&start=<?php echo $start ?>&per_hal=<?php echo $per_hal ?>" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
					<!--table-->
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom header table-->
							<tr>
			          			<th hidden>ID</th>
                                <th>Warehouse Code</th>
                                <th>Warehouse Name</th>
								<th>Product Code</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
				      </tr>

							<?php 

							while($user_data = mysqli_fetch_array($result)) 
							{ 
									echo "<tr>";
                                    echo "<td hidden>".$user_data['Id']."</td>";
                                    echo "<td> <h6>".$user_data['WarehouseCode']."</h6></td>";
									echo "<td> <h6>".$user_data['WarehouseName']."</h6> </td>";
									echo "<td> <h6>".$user_data['ProductCode']."</h6></td>";
									echo "<td> <h6>".$user_data['ProductName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['Quantity']."</h6></td>";
									echo "</tr>";
							}

							?>
							
						</table>
						<ul class="pagination">			
							<?php 
							for($x=1;$x<=$halaman;$x++){
								?>
								<li><a href="?cari=<?php echo $cari?>&page=<?php echo $x ?>"><?php echo $x ?></a></li>
								<?php
							}
							?>						
						</ul>
					</div>
				</div>
			</div>
		</main>
        <!--end of content-->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
			});
	</script>
</body>
</html>