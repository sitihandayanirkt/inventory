<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
require('../assets/pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'PT. ABCD',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. ABCD',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.abcd.com email :abcd@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Stok Barang",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Warehouse', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Warehouse Name', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Product Code', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Product Name', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Quantity', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Satuan', 1, 1, 'C');
$pdf->SetFont('Arial','',10);

$cari = $_GET['cari'];
$start = $_GET['start'];
$per_hal = $_GET['per_hal'];
$no=1;
if($cari != null && $cari != "")
	$sql = "SELECT * FROM stock WHERE ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%' OR WarehouseCode LIKE '%$cari%' OR WarehouseName LIKE '%$cari%'";
else
	$sql = "SELECT * FROM stock";

$query=mysqli_query($koneksi, $sql);
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['WarehouseCode'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['WarehouseName'], 1, 0,'C');
	$pdf->Cell(5, 0.8, $lihat['ProductCode'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['ProductName'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['Quantity'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['UnitName'], 1, 1,'C');

	$no++;
}

$pdf->Output("laporan_stock.pdf","I");

?>

