<?php session_start();
include_once("../config.php");

if( !isset($_SESSION['admin']) )
{
  header('location:./../'.$_SESSION['akses']);
  exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
$valueCari = '';

$dateFrom = date("Y-m-01");
$dateTo = date("Y-m-t");
$per_hal=10;
$jum=1;

if(isset($_GET['dateFrom']))
	$dateFrom = $_GET['dateFrom'];

if(isset($_GET['dateTo']))
	$dateTo = $_GET['dateTo'];

if(isset($_GET['cari']) && $_GET['cari'] != null && $_GET['cari'] != "")
{
	$cari =  $_GET['cari'];
	$valueCari =  $_GET['cari'];
	$sql_record = "SELECT COUNT(*) AS Count FROM lending WHERE (LendingNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR BorrowerCode LIKE '%$cari%' OR BorrowerName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%')
					AND CollectionDate >= '$dateFrom' AND CollectionDate <= '$dateTo'";
}
else
{
	$sql_record = "SELECT COUNT(*) AS Count FROM lending WHERE CollectionDate >= '$dateFrom' AND CollectionDate <= '$dateTo'";
}

$jumlah_record=mysqli_query($koneksi, $sql_record);
while($data = mysqli_fetch_array($jumlah_record)) 
{ 
	if($data['Count'] > 0)
		$jum = $data['Count'];
}
$halaman=ceil($jum / $per_hal);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $per_hal;

$result = mysqli_query($koneksi, "SELECT * FROM lending WHERE CollectionDate >= '$dateFrom' AND CollectionDate <= '$dateTo'
								 limit $start, $per_hal");

if(isset($_POST['cari']))
{
	$cari = $_POST['cari'];
	$valueCari = $cari;
	$dateFrom = $_POST['dateFrom'];
	$dateTo = $_POST['dateTo'];
	$date1=date_create($dateFrom);
	$date2=date_create($dateTo);
	$_dateFrom = date_format($date1,"Y-m-d");
	$_dateTo =  date_format($date2,"Y-m-d");
	$pnjng = 100;
	$substr = substr($cari, 0, $pnjng).' ...';
	if($cari != null && $cari != '')
	{
		$sql = "SELECT * FROM lending WHERE LendingNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR BorrowerCode LIKE '%$cari%' OR BorrowerName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%'
				AND CollectionDate >= '$_dateFrom' AND CollectionDate <= '$_dateTo'
				limit $start, $per_hal";
		$sql_record2 = "SELECT COUNT(*) AS Count FROM lending WHERE (LendingNo LIKE '%$cari%' OR EmployeeName LIKE '%$cari%' OR EmployeeCode LIKE '%$cari%' OR BorrowerCode LIKE '%$cari%' OR BorrowerName LIKE '%$cari%' OR ProductName LIKE '%$cari%' OR ProductCode LIKE '%$cari%')
						AND CollectionDate >= '$_dateFrom' AND CollectionDate <= '$_dateTo'";
	}
	else
	{
		$sql = "SELECT * FROM lending WHERE CollectionDate >= '$_dateFrom' AND CollectionDate <= '$_dateTo'
				limit $start, $per_hal";
		$sql_record2 = "SELECT COUNT(*) AS Count FROM lending WHERE CollectionDate >= '$_dateFrom' AND CollectionDate <= '$_dateTo'";
	}

	$jumlah_record2=mysqli_query($koneksi, $sql_record2);
	while($data = mysqli_fetch_array($jumlah_record2)) 
	{ 
		if($data['Count'] > 0)
			$jum = $data['Count'];
	}
	$halaman=ceil($jum / $per_hal);
	$page = 1;
	$start = 1;

	$result = mysqli_query($koneksi,$sql);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include 'headmenu.php';?>	
</head>
<body>
	<div class="row">
		<!--header-->
		<header>
			<!--TopNav-->
			<nav class="row top-nav red darken-2">
					<div class="container">
							<div class="col offset-l2 nav-wrapper">
									<a href="#" data-activates="slide-out" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
									<a class="page-title">Inventaris</a>
							</div>
					</div>
			</nav>
			<?php include 'sidenav.php';?>
		</header>
		<!--end of header-->

		<!--content-->
		<main>
			<div class="row container">
				<div class="col s12 m12 l12 offset-l2"> <br>
					<!--kolom search-->
					<div class="col s12 m12 l12">
						<form method="post" action="lendings.php" class="row">
							<div class="col s3 m3 l3">
								<input type="date" name="dateFrom" value="<?php echo $dateFrom ?>">
							</div>
							<div class="col s3 m3 l3">
								<input type="date" name="dateTo" value="<?php echo $dateTo ?>">
							</div>
							<div class="e-input-field col s6 m6 l6">
								<input value="<?php echo $valueCari ?>" name="cari" onchange="this.form.submit();" type="text" name="cari" placeholder="Cari Berdasarkan Employee Code / Nama" class="validate" title="Cari User">
								<input type="submit" name="cari2" value="cari" class="btn right red darken-2"> 
							</div>
						</form>
					</div>

					<a  style="margin-bottom:10px" href="report-lending.php?cari=<?php echo $cari ?>&start=<?php echo $start ?>&per_hal=<?php echo $per_hal ?>&dateFrom=<?php echo $dateFrom ?>&dateTo=<?php echo $dateTo ?>" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
					<!--table-->
					<div class="col s12 m12 l12 card-panel z-depth"> <br>
						<table class="highlight">
							<!--kolom header table-->
							<tr>
			          			<th hidden>ID</th>
								<th>Kode</th>
								<th>Tgl Pengambilan</th>
                                <th>Tgl Pengembalian</th>
                                <th>Employee Code</th>
                                <th>Employee Name</th>
                                <th>Borrower Code</th>
                                <th>Borrower Name</th>
								<th>Product Code</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Status</th>
								<th>Action</th>
				      </tr>

							<?php 

							while($user_data = mysqli_fetch_array($result)) 
							{ 
									echo "<tr>";
									echo "<td hidden>".$user_data['Id']."</td>";
                                    echo "<td> <a href='edit-lending.php?id=$user_data[Id]' style='text-decoration: none;'><h6>".$user_data['LendingNo']."</h6></a></td>";
                                    echo "<td> <h6>".$user_data['CollectionDate']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['ReturnDate']."</h6> </td>";
									echo "<td> <h6>".$user_data['EmployeeCode']."</h6></td>";
									echo "<td> <h6>".$user_data['EmployeeName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['BorrowerCode']."</h6></td>";
									echo "<td> <h6>".$user_data['BorrowerName']."</h6> </td>";
									echo "<td> <h6>".$user_data['ProductCode']."</h6></td>";
									echo "<td> <h6>".$user_data['ProductName']."</h6> </td>";
                                    echo "<td> <h6>".$user_data['Quantity']." ".$user_data['UnitName']."</h6></td>";
									echo "<td> <h6>".$user_data['Status']."</h6> </td>";
									echo "<td> <a data-id='1' class='setCompleted' href='completed-lending.php?id=$user_data[Id]' style='text-decoration: none;'>
													<i class='material-icons' title='Completed'>Completed</i>
												</a>
										  </td>";
									echo "</tr>";
							}
							?>
							
						</table>
						<ul class="pagination">			
							<?php 
							for($x=1;$x<=$halaman;$x++){
								?>
								<li><a href="?cari=<?php echo $cari?>&dateFrom=<?php echo $dateFrom ?>&dateTo=<?php echo $dateTo ?>&page=<?php echo $x ?>"><?php echo $x ?></a></li>
								<?php
							}
							?>						
						</ul>
						<table>
							<tr>
								<td colspan='13'>
									<a href='add-lending.php' class="right waves-effect waves-light btn red darken-2">Add <?php echo $string; ?><i class="material-icons right">add</i></a>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</main>
        <!--end of content-->


	</div>

	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('.collapsible').collapsible();
	    	$(".button-collapse").sideNav();
			});
	</script>
	<script>
	    $(".setCompleted").click(function () {
        var jawab = confirm("Apakah Barang Telah Dikembalikan ?");
        if (jawab === true) {
        // konfirmasi
            var hapus = false;
            if (!hapus) {
                hapus = true;
                $.post('completed-lending.php', {id: $(this).attr('data-id')},
                function (data) {
                    // alert(data);
                });
                hapus = false;
            }
        } else {
            return false;
        }
        });
    </script>
</body>
</html>